import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import Person from './Person/Person'

class App extends Component {
  state = {
    persons: [
      {name: 'Max', age: 28},
      {name: 'Manu', age:29},
      {name: 'Ilene', age:30}
    ]
  }

  switchNameHandler = (newName) => {
    //console.log('Was Clicked')
    //DO NOT DO THIS this.state.persons[0].name = 'Superman';
    this.setState({persons:[
      {name: newName, age: 28},
      {name: newName, age:40},
      {name: newName, age:30}
    ]})
  }

  nameChangedHandler = (event) => {
    this.setState({
      persons:[
        {name: 'Max', age: 28},
        {name: event.target.value, age:40},
        {name: 'Ilene', age:30}
      ]
    })
  }
  
  
  render () {
    return (
      <div className="App">
       
        <h1>Hi, I'm a React App named Sue</h1>


        <button onClick={this.switchNameHandler.bind(this, 'Superman')}>Switch Name</button>


        <Person 
          name={this.state.persons[0].name} 
          age={this.state.persons[0].age}/>
        <Person 
          name={this.state.persons[1].name} 
          age={this.state.persons[1].age}
          click={this.switchNameHandler.bind(this, 'Thor')} 
          changed={this.nameChangedHandler}>My Hobbies:Coding</Person>
        <Person 
          name={this.state.persons[2].name} 
          age={this.state.persons[2].age}/>

      </div> 
      );
    }

    //return React.createElement('div',{className: 'App'},React.createElement('h1', null, 'I\'m a React App?!'))
  
}

export default App;
